# VDOM

My playground for various VDOM implementations.

Obviously React is a big one, but React is also a *big* one. Can we get something learner?

- Petit: [GitLab](https://gitlab.com/TomasHubelbauer/bloggo-petit), [Bloggo](http://hubelbauer.net/post/petit)
- Radi: [GitLab](https://gitlab.com/TomasHubelbauer/bloggo-radi), [Bloggo](http://hubelbauer.net/post/radi)
- [ ] [`virtual-dom`](https://github.com/Matt-Esch/virtual-dom)

[Stefan Krauses' benchmark of VDOM implementations](http://www.stefankrause.net/wp/?p=454)

- [ ] Create demos for other performant VDOM implementations.
